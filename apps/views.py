from django.http.response import HttpResponse
from django.shortcuts import render

# Create your views here.
from django.views.generic.base import View


class Home(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'apps/login.html')